#include <Arduino.h>

// epd
#include "epd_driver.h"
#include "epd_highlevel.h"

// deepsleep
#include "esp_sleep.h"

static constexpr int BATTERY_PIN = 36;
static EpdiyHighlevelState hl;
static uint8_t* framebuffer = nullptr;

static int hours = 0;
static int minutes = 0;

void setup() {
  epd_init(EPD_OPTIONS_DEFAULT);
  hl = epd_hl_init(EPD_BUILTIN_WAVEFORM);
  epd_set_rotation(EPD_ROT_LANDSCAPE);
  framebuffer = epd_hl_get_framebuffer(&hl);
  epd_clear();
}

void loop() {
  epd_hl_set_all_white(&hl);
  auto time_string = String(hours) + ":" + String(minutes);
  int width = 0;
  int height = EPD_HEIGHT / 2;
  epd_write_default(&descent, time_string.c_str(), &width, &height, framebuffer);
  epd_poweron();
  epd_hl_update_screen(&hl, MODE_GC16, 20);
  delay(500);
  epd_poweroff();
  delay(1000);
  sleep(60);
  minutes++;
  // put your main code here, to run repeatedly:
}
