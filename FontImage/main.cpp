#include <QApplication>
#include <QPainter>
#include <QPainterPath>
#include <QPixmap>
#include <QRawFont>

static void ClampFontSize(QRawFont &font, int maxWidth, int maxHeight, QString longestString)
{
    do {
        font.setPixelSize(maxHeight);
        --maxHeight;
    } while ([&] {
        auto glyphs = font.glyphIndexesForString(longestString);
        int width = 0;
        for (auto glyph : glyphs) {
            width += font.boundingRect(glyph).width();
        }
        return width > maxWidth;
    }());
}

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    auto height = 540;
    QRawFont font(QString(argv[1]), height);
    ClampFontSize(font, 920, 540, "12:00");
    auto glyphs = font.glyphIndexesForString("0123456789:");
    for (auto glyph : glyphs) {
        auto *image = new QPixmap(font.boundingRect(glyph).width(), height);
        QPainter painter(image);
        painter.fillRect(image->rect(), Qt::white);
        painter.setBrush(Qt::black);
        painter.drawPath(font.pathForGlyph(glyph).translated(0, font.pixelSize()));
        painter.end();
        image->toImage().save(QString("%1.png").arg(glyph));
    }
}
